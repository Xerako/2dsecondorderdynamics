extends Sprite2D

# The following code is an implementation in Godot based on t3ssel8r's demonstration video
# of second-order dynamics "Giving Personality to Procedural Animations using Math."

# target to follow
@onready var target : Sprite2D = $"../Target"

# second order dynamics constants
@export var F : float = 5
@export var Z : float = 0.3
@export var R : float = 0

# second order dynamics variables
var x0 : Vector2
var xp : Vector2
var y : Vector2
var yd : Vector2
var k1 : float
var k2 : float
var k3 : float

# Called when the node enters the scene tree for the first time.
func _ready():
	x0 = target.global_position
	second_order_dynamics(F, Z, R, x0)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	xp = compute(delta, target.global_position, xp)
	global_position = xp

func second_order_dynamics(f : float, z : float, r : float, _x0 : Vector2) -> void:
	# compute constants
	k1 = z / (PI * f)
	k2 = 1 / ((2 * PI * f) * (2 * PI * f))
	k3 = r * z / (2 * PI * f)
	# init variables
	xp = _x0
	y = _x0
	yd = Vector2.ZERO

func compute(T : float, x : Vector2, xd : Vector2) -> Vector2:
	if xd == null:
		xd = (x - xp) / T
		xp = x
	y = y + T * yd
	yd = (k2*yd + T * (x + k3*xd - y)) / (k2 + T*k1)
	return y
