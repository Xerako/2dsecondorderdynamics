# 2DSecondOrderDynamics



## Credits

The following code is an implementation in Godot based on [t3ssel8r](https://www.youtube.com/@t3ssel8r)'s demonstration video
of second-order dynamics ["Giving Personality to Procedural Animations using Math."](https://youtu.be/KPoeNZZ6H4s?si=u8GlbMNuaL9DjiDS)

## Personal Notes

This project implements the Fully-Implicit Euler method instead of the Semi-Implicit Euler method
as detailed in the video.

Some additional information can be found [here](https://www.patreon.com/posts/procedural-video-82487305) under t3ssel8r's patreon.

## Godot Version

Godot *4.2.1* was used for the creation of this project but the code itself is not version dependent.

## Using this Code

If you use this code please credit t3ssel8r, as they are the one who put forward the research
and the knowhow to get an implementation working in their own game.

I simply converted t3ssel8r's code to GDScript and got it working in Godot.

Thank you, t3ssel8r, for being you.

Xerako